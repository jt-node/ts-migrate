import { test } from 'tap'
import { cacheTree } from '../../src/lib'
import { CachedScripts } from '../../src/lib/models'
import { getExportStrategy } from '../../src/factory'
import { MysqlExport, MysqlV5Export } from '../../src/mysql'
import { DatabaseDetails } from '../lib'
const { getFixture } = require('../helper')

test('should throw an error for invalid inputs', async (t) => {
  const scripts: CachedScripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])
  const scenarios = [
    {
      dialect: '',
      connection: {user: '', database:''},
      scripts: {}
    },
    {
      dialect: 'invalid',
      connection: {user: '', database:''},
      scripts: scripts
    },
    {
      dialect: 'mysql',
      connection: {user: 'xyz', database:''},
      scripts: scripts
    },
    {
      dialect: 'mysql',
      connection: {user: 'root', database:'', port:3355},
      scripts: scripts
    }
  ]

  const tests = scenarios.map(async (scenario) => {
    try {
      await getExportStrategy(scenario.dialect, scenario.connection , scenario.scripts)
      t.notOk(true)
    } catch(err) {
      // console.log(err.message)
      t.ok(err)
    }
  })
  await Promise.all(tests)
  t.end()
})

test('Create and close should work.', async (t) => {

  const databases = getFixture('databases')
  const scripts: CachedScripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])

  const tests = databases.map(async (db: DatabaseDetails) => {
    const exporter = await getExportStrategy(db.name, db.connection, scripts)
    await exporter.close()
    t.ok(true)
  })

  await Promise.all(tests)
  t.end()
})
// create and close without init ?

test('should return valid export strategy', async (t) => {

  let scenarios = getFixture('databases')
  const scripts: CachedScripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])
  const tests = scenarios.map(async (scenario:any) => {
    scenario.expected = (scenario.cte) ? MysqlExport: MysqlV5Export
    const exporter = await getExportStrategy(scenario.name, scenario.connection , scripts)
    exporter.close()
    t.ok(exporter instanceof scenario.expected)
  })
  await Promise.all(tests)
  t.end()
})

test('Export schema without init should fail.', async (t) => {
  const databases = getFixture('databases').filter((db: DatabaseDetails) => !db.cte)
  const scripts: CachedScripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])

  const tests = databases.map(async (db: DatabaseDetails) => {
    // const exporter = await getExportStrategy(db.name, db.connection, scripts)
    const exporter = new MysqlV5Export(db.connection, scripts)
    try {
      await exporter.getSchema(['abc'])
    } catch (err) {
      t.ok(err)
    }
    await exporter.close()
  })

  await Promise.all(tests)
  t.end()
})

test('Export schemas from mysql', async (t) => {
  const scripts: CachedScripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])
  const scenarios = getFixture('schema')

  const databases = getFixture('databases')
  const dbtests = databases.map(async (db: DatabaseDetails) =>{
    const exporter = await getExportStrategy(db.name, db.connection, scripts)
    const tests = scenarios.map(async (scenario: any) => {
      const output = await exporter.getSchema(scenario.input.names)
      t.same(output, scenario.output, `${scenario.message} for ${db.name}:${db.version}`)
    })
    await Promise.all(tests)
    await exporter.close()
  })
  await Promise.all(dbtests)
  t.end()
})
