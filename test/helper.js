const fs = require('fs')
const yaml = require('js-yaml')
const path = require('path')

function getFixture(name) {
  return yaml.safeLoad(fs.readFileSync(path.join('test/fixtures/', `${name}.yaml`), 'utf8'))
}

function sleep(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

module.exports = {
  getFixture,
  sleep
}
