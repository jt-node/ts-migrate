export interface DatabaseDetails {
  name: string;
  cte?: boolean;
  version: string;
  connection: {
    port: number;
    database: string;
    user: string;
  }
}
