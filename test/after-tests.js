const { execSync } = require('child_process');
const rimraf = require('rimraf');

function main() {
  const result = execSync('docker-compose down').toString();
  console.log(result)
  rimraf.sync('tmp')
}

main()
