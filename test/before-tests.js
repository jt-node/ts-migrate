const { execSync, spawnSync } = require('child_process');
const { getFixture, sleep } = require('./helper')

// start docker-compose
// wait for services to be available initialize sql
async function initialize(service) {
  const task = `docker ps | grep ${service.name}:${service.version} | grep ${service.connection.port} | wc -l`
  let isUp = false

  while (!isUp) {
    const result = execSync(task).toString()
    isUp = (parseInt(result, 10) === 1)
    await sleep(10000)
  }

  let done = false
  let count = 0
  while (!done && count < 20){
    // console.log(`${service.name}:${service.version} ${count}`)
    try {
      const init = `mysql -u root -P ${service.connection.port} < test/setup.sql 2> /dev/null`
      let result = execSync(init)
      done = true
      console.log(`${service.name}:${service.version} => setup complete after ${count} retries`)
    } catch (err) {
      // console.log(`${service.name}:${service.version} ${err.message}`)
      count += 1
      await sleep(2000)
    }
  }
  if (!done) {
    console.error(`${service.name}:${service.version} => setup failed with ${count} retries`)
  }
}

async function main(){
  const result = execSync(`docker-compose up -d`).toString()
  console.log(result.toString())

  const services = getFixture('databases')
  // await sleep(3000)
  const tasks = services.map(async (service) => {
    await initialize(service)
  })

  await Promise.all(tasks)
}

main()
