import * as pino from "pino"
import config = require('config')


function getLogger() {
  const logCfg = {
    name: 'migrate',
    level: process.env.LOG_LEVEL || 'info'
  }

  if (config.has('logs.dest')) {
    return pino(logCfg, pino.destination(config.get('logs.dest')))
  }
  return pino(logCfg);
}

export const logger = getLogger()

// interface DurationInfo {
//   initiatedAt: Date;
//   completedAt?: Date;
//   duration?: number;
//   data?: any;
//   msg?: string;
// }

export class TaskDuration {
  task: string;
  initiatedAt: Date;
  completedAt?: Date | undefined;
  duration?: number | undefined;
  data?: object;

  constructor(name: string) {
    this.task = name
    this.initiatedAt = new Date()
  }

  done(data?: object): TaskDuration {
    this.completedAt = new Date()
    this.data = data
    this.duration = (this.completedAt.getTime() - this.initiatedAt.getTime()) / 1000
    return this
  }
}

// export class Logger {
//   durationInfo: DurationInfo;
//   logger: any;
//
//   constructor() {
//
//     console.log(dest)
//
//     this.logger = pino({
//       name: 'migrate',
//       level: process.env.LOG_LEVEL || 'info'
//     }, dest)
//
//     this.durationInfo = {
//       initiatedAt: new Date()
//     }
//   }
//
//   init(): void {
//     this.durationInfo = {
//       initiatedAt: new Date()
//     }
//   }
//
//   private calc(data?: any) {
//     this.durationInfo.completedAt = new Date()
//     if (data){
//       this.durationInfo.data = data
//     }
//     this.durationInfo.duration = (this.durationInfo.completedAt.getTime() - this.durationInfo.initiatedAt.getTime()) / 1000
//   }
//
//   warn(msg: string, data?: any): void {
//     this.calc(data)
//     this.logger.warn(this.durationInfo, msg)
//     this.init()
//   }
//   error(msg: string, data?: any): void {
//     this.calc(data)
//     this.logger.error(this.durationInfo, msg)
//   }
//   info(msg: string, data?: any): void {
//     this.calc(data)
//     this.logger.info(this.durationInfo, msg)
//   }
//   debug(msg: string, data?: any): void {
//     this.calc(data)
//     this.logger.debug(this.durationInfo, msg)
//   }
// }

// export const logger = new Logger()
