import fs = require('fs')
import path = require('path')
import { omitBy, isNil} from 'lodash'
import { SchemaWithWarning, CachedScripts } from '../lib/models'
// import * as jq from 'node-jq'
const jq = require('node-jq')

export class ValidationError extends Error {
  data: any;
  errors: string[];

  constructor (message: string, data: any, errors?: string[]) {
    super(message)
    this.name = 'ValidationError'
    this.data = data
    this.errors = errors || []
  }
}

/**
 * Finds difference of two arrays and returns the difference with a message for each item
 *
 * @param {Array} a - Array to be compared to
 * @param {Array} b - Array to be compared with
 * @param {string} message - message to be shown for the difference
 */
export function diff (a: string[], b: string, message: string): SchemaWithWarning[]  {
  const result = a.filter(name => !b.includes(name))
  const info: SchemaWithWarning[] = []
  result.forEach((name) => {
    info.push({name, warning: message})
  })
  return info
}

export function combine(one: string[], two: string[], messages: string[]): SchemaWithWarning[] {
  let result: SchemaWithWarning[]

  result = one.map((name: string) => {
    const item: SchemaWithWarning = {name}
    if (!two.includes(name)){
      item.warning = messages[0]
    }
    return item
  })

  const extra: string[] = two.filter((name: string) => !one.includes(name))
  result = result.concat(
    extra.map((name: string) => {
      return { name, warning: messages[1] }
    })
  )

  return result
}

/**
 * Walk path and read the contents into an object tree to be used as a shared cache.
 *
 * @param {string} dir - path to be recursively scanned
 * @param {string or Array} include - regex pattern to be matched or an array of file extensions
 *        used for identifying which files are included in the scan. Default includes all files.
 */
export function cacheTree (dir: string = '.', include: any): CachedScripts {
  let tree: {[key: string]: any} = {}
  let pattern = '.+'

  if (!fs.existsSync(dir)) {
    throw new ValidationError('Specified path does not exist.', { dir, include })
  }
  if (!fs.statSync(dir).isDirectory()) {
    throw new ValidationError('Specified path is not a directory.', { dir, include })
  }

  const files = fs.readdirSync(dir)

  if (Array.isArray(include)) {
    pattern = `(${include.join('|')})$`
  } else if (typeof (include) === 'string') {
    pattern = include
  }

  files.forEach((filename: string) => {
    const filepath = path.join(dir, filename)

    const key: string = path.basename(filename, path.extname(filename))
    if (fs.statSync(filepath).isDirectory()) {
      tree[key] = cacheTree(filepath, pattern)
    } else if (path.extname(filename).match(pattern)) {
      tree[key] = fs.readFileSync(filepath).toString('utf8')
    }
  })
  tree = omitBy(tree, isNil)
  return (Object.keys(tree).length === 0) ? {} : tree
}

/**
 * Sleeps for a given duration in milliseconds
 *
 * @param {number} milliseconds - milliseconds to sleep
 */
export async function sleep(milliseconds: number) {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

/**
 * Transforms a json object using a `jq` filter script.
 *
 * @param {string} filter - jq filter script to be applied
 * @param {any} json - json object on which the filter is applied
 */
export async function transform(filter: string, json: any): Promise<any> {
  if (Array.isArray(json)){
    return jq.run(filter, JSON.stringify(json), {input: 'string', output: 'json'})
  } else {
    return jq.run(filter, json, {input: 'json', output: 'json'})
  }

}
