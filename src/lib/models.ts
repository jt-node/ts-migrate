export interface ColumnInfo {
  name: string
  type: string
  position: number
  precision: number
  scale: number
  default: number
  comment: string
}

export interface IndexColumnInfo {
  name: string
  position: number
  ascending?: boolean
}

export interface ReferenceColumnInfo {
  name: string
  target: string
  position: number
  ascending: boolean
}

export interface Table {
  schema: string
  table: string
}

export interface SchemaWithWarning {
  name: string
  warning?: string
}
export interface TableWithPriority extends Table {
  priority: number
}
export interface ReferenceInfo {
  schema: string
  name: string
  references: Table
  columns: ReferenceColumnInfo[]
}

export interface IndexInfo {
  schema: string
  name: string
  uniqueness: boolean
  columns: IndexColumnInfo[]
}

export interface TableInfo {
  schema: string
  table: string
  comment?: string
  columns: ColumnInfo[]
  indexes: IndexInfo[]
  key: IndexInfo[]
  // references: ReferenceInfo[]
}

export interface SchemaData {
  schemas: SchemaWithWarning[]
  groups: [TableWithPriority[]]
}

export interface ConnectionInfo {
  user: string;
  password?: string;
  host?: string;
  database: string;
  // dialect: string;
  // version: string;
}

export interface CachedScripts {
  [key: string] : Record<string, any>
}
export interface ConfigInfo {
  connection: ConnectionInfo;
  scripts: CachedScripts;
}

export interface TableStream {
  schema: string;
  table: string;
  stream: any;
}
