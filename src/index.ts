import { MigrationContext } from './context'
import { getExportStrategy } from './factory'
import { CachedScripts } from './lib/models'
import { cacheTree } from './lib'
import { MysqlImport } from './mysql'
import { logger } from './lib/logger'
/**
 * The client code picks a concrete strategy and passes it to the context. The
 * client should be aware of the differences between strategies in order to make
 * the right choice.
 */

const scripts: CachedScripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])
const v8Connection = {
  user: 'root',
  database: 'test_migrate',
  version: '5.0.0'
}

// const v5Connection = {
//   user: 'root',
//   database: 'test_migrate',
//   version: '5',
//   dialect: 'mysql'
// }


// const contextB = new MigrationContext(new MysqlV5Export(), new MysqlImport());
// console.log('Client: Migrating from X to Y');
// contextB.migrateSchema(['a','b','c']);
async function main(){
  const exportStrategy = await getExportStrategy('mysql', v8Connection, scripts)
  const context = new MigrationContext(exportStrategy, new MysqlImport())

  logger.info('Client: Migrating from X to Y')
  await context.migrateSchema(['exp_data'])
  // await context.migrateTable('exp_data','no_fk_data')
  await context.migrateData('exp_data', 'no_fk_data')
  logger.info({ schema: 'exp_data', table: 'no_fk_data' }, "Exporting table")
  logger.info('closing all connections')
  await context.close()
}

main()
