import * as semver from 'semver'
import { CachedScripts, ConnectionInfo } from './lib/models'
import { ExportStrategy } from './strategy'
import { MysqlExport, MysqlV5Export} from './mysql'
// import { sleep } from './lib'

const mysqlPromise = require('mysql2/promise')

async function getMysqlVersion(connection: ConnectionInfo): Promise<string> {
  const pool = mysqlPromise.createPool(connection)
  const [rows] = await pool.query("show variables like 'version';")
  await pool.end()
  return rows[0].Value
}

// factory should validate connection
// it should also check that database is specified for v5 cases.
export async function getExportStrategy(dialect: string, connection: ConnectionInfo, scripts: CachedScripts): Promise<ExportStrategy> {

  const diff = ['ddl', 'jq', 'sql'].filter(x => !Object.keys(scripts).includes(x))
  if (diff.length > 0) {
    throw new Error('Required scripts are not loaded')
  }

  if (['mariadb', 'mysql'].includes(dialect)) {
    let cteMinVersion = '8.0.0'
    let version = await getMysqlVersion(connection)
    let exporter: ExportStrategy

    if (dialect === 'mariadb') {
      cteMinVersion = '10.2.2'
      version = version.split('-')[0]
    }

    if (semver.lt(version, cteMinVersion)){
      if (!connection.database || connection.database.trim() === ''){
        throw new Error(`A valid database is required for export for ${dialect} [${version}]`)
      } else {
        exporter = new MysqlV5Export(connection, scripts)
      }
    } else {
      exporter = new MysqlExport(connection, scripts)
    }
    await exporter.init()
    return exporter
  }

  throw new Error(`No valid export strategies found for [${dialect}]`)
}
