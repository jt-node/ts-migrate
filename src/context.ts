import { ExportStrategy, ImportStrategy } from './strategy'
import { SchemaData, TableInfo } from './lib/models'
import { logger as appLogger, TaskDuration } from './lib/logger'

const logger = appLogger.child({class: 'MigrationContext'})

export class MigrationContext {
  /**
   * @type {ExportStrategy} The Context maintains a reference to one of the Strategy
   * objects. The Context does not know the concrete class of a strategy. It
   * should work with all strategies via the Strategy interface.
   *
   * @type {ImportStrategy} The Context maintains a reference to one of the Strategy
   * objects. The Context does not know the concrete class of a strategy. It
   * should work with all strategies via the Strategy interface.
   */
  private source: ExportStrategy
  private target: ImportStrategy
  // private readonly scripts: CachedScripts
  /**
   * Usually, the Context accepts a strategy through the constructor, but also
   * provides a setter to change it at runtime.
   */
  constructor (source: ExportStrategy, target: ImportStrategy) {

    this.source = source
    this.target = target

    // this.scripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])

  }

  /**
   * Usually, the Context allows replacing a Strategy object at runtime.
   */
  public setExportStrategy (source: ExportStrategy): void {
    this.source = source
  }

  public setImportStrategy (target: ImportStrategy): void {
    this.target = target
  }

  public async migrateData(schema: string, table: string): Promise<void>{
    await this.source.getData(schema, table, (data: any, done) => {
      logger.info('row received', data)
      done()
    })
    logger.info('Completed reseiving all data')
  }

  public async migrateTable(schema: string, table: string): Promise<void>{
    const task = new TaskDuration('migrateTable')
    const result: TableInfo = await this.source.getTable(schema, table)
    logger.info(task.done({schema, table, result}), 'received structure')
  }
  /**
   * The Context delegates some work to the Strategy object instead of
   * implementing multiple versions of the algorithm on its own.
   */
  public async migrateSchema (names: string[]): Promise<void> {
    const task = new TaskDuration('migrateTable')
    const result: SchemaData = await this.source.getSchema(names)
    logger.info(task.done({names, result}), 'Schema details')
    this.target.putSchema(result)
  }

  public async close(): Promise<void> {
    await this.source.close()
  }
}
