import * as mysqlCallback from 'mysql2'
import * as mysqlPromise from 'mysql2/promise'
import { ExportStrategy } from '../strategy'
import { sleep, combine, transform } from '../lib'
import { logger as appLogger, TaskDuration } from '../lib/logger'
import { SchemaData, TableInfo, CachedScripts } from '../lib/models'
// import { PoolOptions } from 'mysql2/promise'


const logger = appLogger.child({module: 'mysql', class: 'MysqlExport'})
/**
 * Concrete Strategies implement the algorithm while following the base Strategy
 * interface. The interface makes them interchangeable in the Context.
 */
export class MysqlExport implements ExportStrategy {

  protected db: any
  protected jq: any
  protected ddl: any
  protected sql: any
  protected connection: mysqlPromise.PoolOptions
  protected streamingData: boolean = false
  // protected initialized: boolean = false

  constructor(connection: mysqlPromise.PoolOptions, scripts: CachedScripts) {
    this.jq = scripts.jq
    this.sql = scripts.sql.mysql
    this.ddl = scripts.ddl.mysql
    this.db = mysqlPromise.createPool(connection)
    this.connection = connection
  }
  public async init(): Promise<void> {
    return
  }

  public async close(): Promise<void> {
    const task = new TaskDuration('close')
    await this.db.end()
    logger.info(task.done(), 'Closed connection pool')
  }

  public async getSchema (names: string[]): Promise<SchemaData> {
    const task = new TaskDuration('getSchema')

    const [rows] = await this.db.query(this.sql.tables, [names])
    const result = await transform(this.jq.group_by_priority, rows)
    const schemas = result.schemas.map((item: any) => { return item.name })
    result.schemas = combine(names, schemas, ['No tables to export', 'Included due to dependency'])

    logger.info(task.done({input: names, output: result}), "fetched objects to be exported")

    return result
  }

  public async getTable (schema: string, table: string): Promise<TableInfo> {
    const task = new TaskDuration('getTable')

    const [columns] = await this.db.query(this.sql.columns, [schema, table])
    const [key] = await this.db.query(this.sql.key, [schema, table])
    const [indexes] = await this.db.query(this.sql.indexes, [schema, table])

    // clean up key names
    const result = await transform(this.jq.table, { schema, table, columns, key, indexes })
    logger.info(task.done({input: {schema, table}, output: result}), "fetched table structure")
    return result
  }

  public async getData (schema: string, table: string, cb: (data: any, done: () => void) => void): Promise<void> {
    const task = new TaskDuration('getData')

    const [[query]] = await this.db.query(this.sql.data, [schema, table])
    const conn = mysqlCallback.createConnection(this.connection)
    const stream = conn.query(query.script)
    let rowCount = 0

    this.streamingData = true
    stream
      .on('error', (err: Error) => {
        logger.error("Unexpected error while streaming data", {err, schema, table})
        conn.end()
        this.streamingData = false
      })
      .on('result', (row: any) => {
        // Pausing the connnection is useful if your processing involves I/O
        conn.pause();
        rowCount += 1
        cb(row, () => {
          conn.resume();
        });
      })
      .on('end', () => {
        // all rows have been received
        this.streamingData = false
        conn.end()
      });

    // Wait for stream processing to be complete, because the data is being sent row by row via callback.
    while (this.streamingData) {
      await sleep(1000)
    }
    logger.info(task.done({input: {schema, table}, output: rowCount}), "fetched table data")
  }
}
