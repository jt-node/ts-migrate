import { ImportStrategy } from '../strategy'
import { SchemaData, TableInfo } from '../lib/models'

export class MysqlImport implements ImportStrategy {
  // public init (): void { }

  public putSchema (data: SchemaData): any {
    return data
  }

  public putTable (data: TableInfo): any {
    return data
  }

  public putData (schema: string, table: string): any {
    return { schema, table }
  }
  public async close(): Promise<void> {
    return;
  }
}
