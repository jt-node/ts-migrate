import { PoolOptions } from 'mysql2/promise'
import { v4 as uuid_v4 } from 'uuid'
import { MysqlExport } from './export'
import { SchemaData, CachedScripts } from '../lib/models'
import { combine, transform } from '../lib'
import { logger as appLogger, TaskDuration } from '../lib/logger'

const logger = appLogger.child({module: 'mysql', class: 'MysqlV5Export'})

export class MysqlV5Export extends MysqlExport {

  protected initialized: boolean = false
  // protected initStarted: boolean = false

  constructor(connection: PoolOptions, scripts: CachedScripts) {
    super(connection, scripts)
  }

  // can't call init in constructor as it is an async function
  // Calling it may cause connection issue which will not be caught during the constructor call
  async init(): Promise<void> {
    // this.initStarted = true

    const task = new TaskDuration('init')

    await this.db.query(this.ddl.drop.get_tables)
    await this.db.query(this.ddl.drop.export_tables)
    await this.db.query(this.ddl.drop.export_session)

    await this.db.query(this.ddl.create.export_session)
    await this.db.query(this.ddl.create.export_tables)
    await this.db.query(this.ddl.create.get_tables)

    this.initialized = true
    // this.initStarted = false
    logger.info(task.done(), "created tables and stored procedure")
  }

  public async getSchema (names: string[]): Promise<SchemaData> {
    const task = new TaskDuration('getSchema')
    const sessionId = uuid_v4()

    if (!this.initialized) {
      throw new Error('init should be called before calling getSchema')
    }

    const doInsertSchemas = names.map(async (name) => {
      return this.db.query(this.sql.insert.export_session, [sessionId, name])
    })

    await Promise.all(doInsertSchemas)
    const [[rows]] = await this.db.query(this.sql.get_tables, [sessionId])

    const result = await transform(this.jq.group_by_priority, rows)

    const schemas = result.schemas.map((item: any) => { return item.name })
    result.schemas = combine(names, schemas, ['No tables to export', 'Included due to dependency'])

    logger.info(task.done({input: names, output: result}), "fetched objects to be exported")
    return result
  }

  public async close(){
    const task = new TaskDuration('close')
    // while (this.initStarted && !this.initialized){
    //   await sleep(100)
    // }
    const waitedUntil = new Date()

    await this.db.query(this.ddl.drop.get_tables)
    await this.db.query(this.ddl.drop.export_tables)
    await this.db.query(this.ddl.drop.export_session)

    await this.db.end()
    logger.info(task.done({waitedUntil}), 'Cleaned up objects created during initialization')
  }
}
