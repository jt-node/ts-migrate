declare module "node-jq" {
  export const optionDefaults: IOptions;
  // override the definition as it is missing input
  interface IOptions {
      color: boolean,
      input: "file" | "json" | "string",
      locations: string[],
      output: 'pretty' | 'json' | 'compact' | 'string'
      raw: boolean,
      slurp: boolean,
      sort: boolean,
  }
  export type PartialOptions = Partial<IOptions>
  export function run(filter: string, json: any, options?: PartialOptions, jqPath?: string, cwd?: string): Promise<object | string>
}
