select object_type
     , object_name
     , object_schema
  from (
    select 'TABLE'        as object_type
         , table_name     as object_name
         , table_schema   as object_schema
      from information_schema.tables
     union all
    select routine_type   as object_type
         , routine_name   as object_name
         , routine_schema as object_schema
      from information_schema.routines
  ) as x
where object_schema = ?
order by object_type
       , object_name
