"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var semver = require("semver");
var mysql_1 = require("./mysql");
// import { sleep } from './lib'
var mysqlPromise = require('mysql2/promise');
function getMysqlVersion(connection) {
    return __awaiter(this, void 0, void 0, function () {
        var pool, _a, rows;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    pool = mysqlPromise.createPool(connection);
                    return [4 /*yield*/, pool.query("show variables like 'version';")];
                case 1:
                    _a = __read.apply(void 0, [_b.sent(), 1]), rows = _a[0];
                    return [4 /*yield*/, pool.end()];
                case 2:
                    _b.sent();
                    return [2 /*return*/, rows[0].Value];
            }
        });
    });
}
// factory should validate connection
// it should also check that database is specified for v5 cases.
function getExportStrategy(dialect, connection, scripts) {
    return __awaiter(this, void 0, void 0, function () {
        var diff, cteMinVersion, version, exporter;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    diff = ['ddl', 'jq', 'sql'].filter(function (x) { return !Object.keys(scripts).includes(x); });
                    if (diff.length > 0) {
                        throw new Error('Required scripts are not loaded');
                    }
                    if (!['mariadb', 'mysql'].includes(dialect)) return [3 /*break*/, 3];
                    cteMinVersion = '8.0.0';
                    return [4 /*yield*/, getMysqlVersion(connection)];
                case 1:
                    version = _a.sent();
                    exporter = void 0;
                    if (dialect === 'mariadb') {
                        cteMinVersion = '10.2.2';
                        version = version.split('-')[0];
                    }
                    if (semver.lt(version, cteMinVersion)) {
                        if (!connection.database || connection.database.trim() === '') {
                            throw new Error("A valid database is required for export for " + dialect + " [" + version + "]");
                        }
                        else {
                            exporter = new mysql_1.MysqlV5Export(connection, scripts);
                        }
                    }
                    else {
                        exporter = new mysql_1.MysqlExport(connection, scripts);
                    }
                    return [4 /*yield*/, exporter.init()];
                case 2:
                    _a.sent();
                    return [2 /*return*/, exporter];
                case 3: throw new Error("No valid export strategies found for [" + dialect + "]");
            }
        });
    });
}
exports.getExportStrategy = getExportStrategy;
