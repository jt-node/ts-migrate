import { PoolOptions } from 'mysql2/promise';
import { MysqlExport } from './export';
import { SchemaData, CachedScripts } from '../lib/models';
export declare class MysqlV5Export extends MysqlExport {
    protected initialized: boolean;
    protected initStarted: boolean;
    constructor(connection: PoolOptions, scripts: CachedScripts);
    init(): Promise<void>;
    getSchema(names: string[]): Promise<SchemaData>;
    close(): Promise<void>;
}
