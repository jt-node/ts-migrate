"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var export_1 = require("./export");
var lib_1 = require("../lib");
var logger_1 = require("../lib/logger");
var logger = logger_1.logger.child({ module: 'mysql', class: 'MysqlV5Export' });
var MysqlV5Export = /** @class */ (function (_super) {
    __extends(MysqlV5Export, _super);
    function MysqlV5Export(connection, scripts) {
        var _this = _super.call(this, connection, scripts) || this;
        _this.initialized = false;
        _this.initStarted = false;
        return _this;
    }
    // can't call init in constructor as it is an async function
    // Calling it may cause connection issue which will not be caught during the constructor call
    MysqlV5Export.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var task;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.initStarted = true;
                        task = new logger_1.TaskDuration('init');
                        return [4 /*yield*/, this.db.query(this.ddl.drop.get_tables)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.drop.export_tables)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.drop.export_session)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.create.export_session)];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.create.export_tables)];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.create.get_tables)];
                    case 6:
                        _a.sent();
                        this.initialized = true;
                        this.initStarted = false;
                        logger.info(task.done(), "created tables and stored procedure");
                        return [2 /*return*/];
                }
            });
        });
    };
    MysqlV5Export.prototype.getSchema = function (names) {
        return __awaiter(this, void 0, void 0, function () {
            var task, sessionId, doInsertSchemas, _a, _b, rows, result, schemas;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        task = new logger_1.TaskDuration('getSchema');
                        sessionId = uuid_1.v4();
                        if (!this.initialized) {
                            throw new Error('init should be called before calling getSchema');
                        }
                        doInsertSchemas = names.map(function (name) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                return [2 /*return*/, this.db.query(this.sql.insert.export_session, [sessionId, name])];
                            });
                        }); });
                        return [4 /*yield*/, Promise.all(doInsertSchemas)];
                    case 1:
                        _c.sent();
                        return [4 /*yield*/, this.db.query(this.sql.get_tables, [sessionId])];
                    case 2:
                        _a = __read.apply(void 0, [_c.sent(), 1]), _b = __read(_a[0], 1), rows = _b[0];
                        return [4 /*yield*/, lib_1.transform(this.jq.group_by_priority, rows)];
                    case 3:
                        result = _c.sent();
                        schemas = result.schemas.map(function (item) { return item.name; });
                        result.schemas = lib_1.combine(names, schemas, ['No tables to export', 'Included due to dependency']);
                        logger.info(task.done({ input: names, output: result }), "fetched objects to be exported");
                        return [2 /*return*/, result];
                }
            });
        });
    };
    MysqlV5Export.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            var task, waitedUntil;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        task = new logger_1.TaskDuration('close');
                        _a.label = 1;
                    case 1:
                        if (!(this.initStarted && !this.initialized)) return [3 /*break*/, 3];
                        return [4 /*yield*/, lib_1.sleep(100)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 1];
                    case 3:
                        waitedUntil = new Date();
                        return [4 /*yield*/, this.db.query(this.ddl.drop.get_tables)];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.drop.export_tables)];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, this.db.query(this.ddl.drop.export_session)];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, this.db.end()];
                    case 7:
                        _a.sent();
                        logger.info(task.done({ waitedUntil: waitedUntil }), 'Cleaned up objects created during initialization');
                        return [2 /*return*/];
                }
            });
        });
    };
    return MysqlV5Export;
}(export_1.MysqlExport));
exports.MysqlV5Export = MysqlV5Export;
