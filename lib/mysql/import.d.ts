import { ImportStrategy } from '../strategy';
import { SchemaData, TableInfo } from '../lib/models';
export declare class MysqlImport implements ImportStrategy {
    putSchema(data: SchemaData): any;
    putTable(data: TableInfo): any;
    putData(schema: string, table: string): any;
    close(): Promise<void>;
}
