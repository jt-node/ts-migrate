import { ExportStrategy } from '../strategy';
import { SchemaData, TableInfo, CachedScripts } from '../lib/models';
import { PoolOptions } from 'mysql2/promise';
/**
 * Concrete Strategies implement the algorithm while following the base Strategy
 * interface. The interface makes them interchangeable in the Context.
 */
export declare class MysqlExport implements ExportStrategy {
    protected db: any;
    protected jq: any;
    protected ddl: any;
    protected sql: any;
    protected connection: PoolOptions;
    protected streamingData: boolean;
    constructor(connection: PoolOptions, scripts: CachedScripts);
    init(): Promise<void>;
    close(): Promise<void>;
    getSchema(names: string[]): Promise<SchemaData>;
    getTable(schema: string, table: string): Promise<TableInfo>;
    getData(schema: string, table: string, cb: (data: any, done: () => void) => void): Promise<void>;
}
