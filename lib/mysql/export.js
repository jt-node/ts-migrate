"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mysqlCallback = require("mysql2");
var mysqlPromise = require("mysql2/promise");
var lib_1 = require("../lib");
var logger_1 = require("../lib/logger");
var logger = logger_1.logger.child({ module: 'mysql', class: 'MysqlExport' });
/**
 * Concrete Strategies implement the algorithm while following the base Strategy
 * interface. The interface makes them interchangeable in the Context.
 */
var MysqlExport = /** @class */ (function () {
    // protected initialized: boolean = false
    function MysqlExport(connection, scripts) {
        this.streamingData = false;
        this.jq = scripts.jq;
        this.sql = scripts.sql.mysql;
        this.ddl = scripts.ddl.mysql;
        this.db = mysqlPromise.createPool(connection);
        this.connection = connection;
    }
    MysqlExport.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    MysqlExport.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            var task;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        task = new logger_1.TaskDuration('close');
                        return [4 /*yield*/, this.db.end()];
                    case 1:
                        _a.sent();
                        logger.info(task.done(), 'Closed connection pool');
                        return [2 /*return*/];
                }
            });
        });
    };
    MysqlExport.prototype.getSchema = function (names) {
        return __awaiter(this, void 0, void 0, function () {
            var task, _a, rows, result, schemas;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        task = new logger_1.TaskDuration('getSchema');
                        return [4 /*yield*/, this.db.query(this.sql.tables, [names])];
                    case 1:
                        _a = __read.apply(void 0, [_b.sent(), 1]), rows = _a[0];
                        return [4 /*yield*/, lib_1.transform(this.jq.group_by_priority, rows)];
                    case 2:
                        result = _b.sent();
                        schemas = result.schemas.map(function (item) { return item.name; });
                        result.schemas = lib_1.combine(names, schemas, ['No tables to export', 'Included due to dependency']);
                        logger.info(task.done({ input: names, output: result }), "fetched objects to be exported");
                        return [2 /*return*/, result];
                }
            });
        });
    };
    MysqlExport.prototype.getTable = function (schema, table) {
        return __awaiter(this, void 0, void 0, function () {
            var task, _a, columns, _b, key, _c, indexes, result;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        task = new logger_1.TaskDuration('getTable');
                        return [4 /*yield*/, this.db.query(this.sql.columns, [schema, table])];
                    case 1:
                        _a = __read.apply(void 0, [_d.sent(), 1]), columns = _a[0];
                        return [4 /*yield*/, this.db.query(this.sql.key, [schema, table])];
                    case 2:
                        _b = __read.apply(void 0, [_d.sent(), 1]), key = _b[0];
                        return [4 /*yield*/, this.db.query(this.sql.indexes, [schema, table])
                            // clean up key names
                        ];
                    case 3:
                        _c = __read.apply(void 0, [_d.sent()
                            // clean up key names
                            , 1]), indexes = _c[0];
                        return [4 /*yield*/, lib_1.transform(this.jq.table, { schema: schema, table: table, columns: columns, key: key, indexes: indexes })];
                    case 4:
                        result = _d.sent();
                        logger.info(task.done({ input: { schema: schema, table: table }, output: result }), "fetched table structure");
                        return [2 /*return*/, result];
                }
            });
        });
    };
    MysqlExport.prototype.getData = function (schema, table, cb) {
        return __awaiter(this, void 0, void 0, function () {
            var task, _a, _b, query, conn, stream, rowCount;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        task = new logger_1.TaskDuration('getData');
                        return [4 /*yield*/, this.db.query(this.sql.data, [schema, table])];
                    case 1:
                        _a = __read.apply(void 0, [_c.sent(), 1]), _b = __read(_a[0], 1), query = _b[0];
                        conn = mysqlCallback.createConnection(this.connection);
                        stream = conn.query(query.script);
                        rowCount = 0;
                        this.streamingData = true;
                        stream
                            .on('error', function (err) {
                            logger.error("Unexpected error while streaming data", { err: err, schema: schema, table: table });
                            conn.end();
                            _this.streamingData = false;
                        })
                            .on('result', function (row) {
                            // Pausing the connnection is useful if your processing involves I/O
                            conn.pause();
                            rowCount += 1;
                            cb(row, function () {
                                conn.resume();
                            });
                        })
                            .on('end', function () {
                            // all rows have been received
                            _this.streamingData = false;
                            conn.end();
                        });
                        _c.label = 2;
                    case 2:
                        if (!this.streamingData) return [3 /*break*/, 4];
                        return [4 /*yield*/, lib_1.sleep(1000)];
                    case 3:
                        _c.sent();
                        return [3 /*break*/, 2];
                    case 4:
                        logger.info(task.done({ input: { schema: schema, table: table }, output: rowCount }), "fetched table data");
                        return [2 /*return*/];
                }
            });
        });
    };
    return MysqlExport;
}());
exports.MysqlExport = MysqlExport;
