import { ExportStrategy, ImportStrategy } from './strategy';
export declare class MigrationContext {
    /**
     * @type {ExportStrategy} The Context maintains a reference to one of the Strategy
     * objects. The Context does not know the concrete class of a strategy. It
     * should work with all strategies via the Strategy interface.
     *
     * @type {ImportStrategy} The Context maintains a reference to one of the Strategy
     * objects. The Context does not know the concrete class of a strategy. It
     * should work with all strategies via the Strategy interface.
     */
    private source;
    private target;
    /**
     * Usually, the Context accepts a strategy through the constructor, but also
     * provides a setter to change it at runtime.
     */
    constructor(source: ExportStrategy, target: ImportStrategy);
    /**
     * Usually, the Context allows replacing a Strategy object at runtime.
     */
    setExportStrategy(source: ExportStrategy): void;
    setImportStrategy(target: ImportStrategy): void;
    migrateData(schema: string, table: string): Promise<void>;
    migrateTable(schema: string, table: string): Promise<void>;
    /**
     * The Context delegates some work to the Strategy object instead of
     * implementing multiple versions of the algorithm on its own.
     */
    migrateSchema(names: string[]): Promise<void>;
    close(): Promise<void>;
}
