"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("./lib/logger");
var logger = logger_1.logger.child({ class: 'MigrationContext' });
var MigrationContext = /** @class */ (function () {
    // private readonly scripts: CachedScripts
    /**
     * Usually, the Context accepts a strategy through the constructor, but also
     * provides a setter to change it at runtime.
     */
    function MigrationContext(source, target) {
        this.source = source;
        this.target = target;
        // this.scripts = cacheTree('scripts', ['jq', 'sql', 'ddl'])
    }
    /**
     * Usually, the Context allows replacing a Strategy object at runtime.
     */
    MigrationContext.prototype.setExportStrategy = function (source) {
        this.source = source;
    };
    MigrationContext.prototype.setImportStrategy = function (target) {
        this.target = target;
    };
    MigrationContext.prototype.migrateData = function (schema, table) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.source.getData(schema, table, function (data, done) {
                            logger.info('row received', data);
                            done();
                        })];
                    case 1:
                        _a.sent();
                        logger.info('Completed reseiving all data');
                        return [2 /*return*/];
                }
            });
        });
    };
    MigrationContext.prototype.migrateTable = function (schema, table) {
        return __awaiter(this, void 0, void 0, function () {
            var task, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        task = new logger_1.TaskDuration('migrateTable');
                        return [4 /*yield*/, this.source.getTable(schema, table)];
                    case 1:
                        result = _a.sent();
                        logger.info(task.done({ schema: schema, table: table, result: result }), 'received structure');
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * The Context delegates some work to the Strategy object instead of
     * implementing multiple versions of the algorithm on its own.
     */
    MigrationContext.prototype.migrateSchema = function (names) {
        return __awaiter(this, void 0, void 0, function () {
            var task, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        task = new logger_1.TaskDuration('migrateTable');
                        return [4 /*yield*/, this.source.getSchema(names)];
                    case 1:
                        result = _a.sent();
                        logger.info(task.done({ names: names, result: result }), 'Schema details');
                        this.target.putSchema(result);
                        return [2 /*return*/];
                }
            });
        });
    };
    MigrationContext.prototype.close = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.source.close()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return MigrationContext;
}());
exports.MigrationContext = MigrationContext;
