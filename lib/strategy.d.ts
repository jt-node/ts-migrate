import { SchemaData, TableInfo } from './lib/models';
export interface ExportStrategy {
    init(): Promise<void>;
    getSchema(names: string[]): Promise<SchemaData>;
    getTable(schema: string, table: string): Promise<TableInfo>;
    getData(schema: string, table: string, cb: (data: any, done: () => void) => void): Promise<void>;
    close(): Promise<void>;
}
export interface ImportStrategy {
    putSchema(data: SchemaData): any;
    putTable(data: TableInfo): any;
    putData(schema: string, table: string, data: any): any;
    close(): Promise<void>;
}
