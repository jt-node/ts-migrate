"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var lodash_1 = require("lodash");
// import * as jq from 'node-jq'
var jq = require('node-jq');
var ValidationError = /** @class */ (function (_super) {
    __extends(ValidationError, _super);
    function ValidationError(message, data, errors) {
        var _this = _super.call(this, message) || this;
        _this.name = 'ValidationError';
        _this.data = data;
        _this.errors = errors || [];
        return _this;
    }
    return ValidationError;
}(Error));
exports.ValidationError = ValidationError;
/**
 * Finds difference of two arrays and returns the difference with a message for each item
 *
 * @param {Array} a - Array to be compared to
 * @param {Array} b - Array to be compared with
 * @param {string} message - message to be shown for the difference
 */
function diff(a, b, message) {
    var result = a.filter(function (name) { return !b.includes(name); });
    var info = [];
    result.forEach(function (name) {
        info.push({ name: name, warning: message });
    });
    return info;
}
exports.diff = diff;
function combine(one, two, messages) {
    var result;
    result = one.map(function (name) {
        var item = { name: name };
        if (!two.includes(name)) {
            item.warning = messages[0];
        }
        return item;
    });
    var extra = two.filter(function (name) { return !one.includes(name); });
    result = result.concat(extra.map(function (name) {
        return { name: name, warning: messages[1] };
    }));
    return result;
}
exports.combine = combine;
/**
 * Walk path and read the contents into an object tree to be used as a shared cache.
 *
 * @param {string} dir - path to be recursively scanned
 * @param {string or Array} include - regex pattern to be matched or an array of file extensions
 *        used for identifying which files are included in the scan. Default includes all files.
 */
function cacheTree(dir, include) {
    if (dir === void 0) { dir = '.'; }
    var tree = {};
    var pattern = '.+';
    if (!fs.existsSync(dir)) {
        throw new ValidationError('Specified path does not exist.', { dir: dir, include: include });
    }
    if (!fs.statSync(dir).isDirectory()) {
        throw new ValidationError('Specified path is not a directory.', { dir: dir, include: include });
    }
    var files = fs.readdirSync(dir);
    if (Array.isArray(include)) {
        pattern = "(" + include.join('|') + ")$";
    }
    else if (typeof (include) === 'string') {
        pattern = include;
    }
    files.forEach(function (filename) {
        var filepath = path.join(dir, filename);
        var key = path.basename(filename, path.extname(filename));
        if (fs.statSync(filepath).isDirectory()) {
            tree[key] = cacheTree(filepath, pattern);
        }
        else if (path.extname(filename).match(pattern)) {
            tree[key] = fs.readFileSync(filepath).toString('utf8');
        }
    });
    tree = lodash_1.omitBy(tree, lodash_1.isNil);
    return (Object.keys(tree).length === 0) ? {} : tree;
}
exports.cacheTree = cacheTree;
/**
 * Sleeps for a given duration in milliseconds
 *
 * @param {number} milliseconds - milliseconds to sleep
 */
function sleep(milliseconds) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, new Promise(function (resolve) { return setTimeout(resolve, milliseconds); })];
        });
    });
}
exports.sleep = sleep;
/**
 * Transforms a json object using a `jq` filter script.
 *
 * @param {string} filter - jq filter script to be applied
 * @param {any} json - json object on which the filter is applied
 */
function transform(filter, json) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (Array.isArray(json)) {
                return [2 /*return*/, jq.run(filter, JSON.stringify(json), { input: 'string', output: 'json' })];
            }
            else {
                return [2 /*return*/, jq.run(filter, json, { input: 'json', output: 'json' })];
            }
            return [2 /*return*/];
        });
    });
}
exports.transform = transform;
