import { SchemaWithWarning, CachedScripts } from '../lib/models';
export declare class ValidationError extends Error {
    data: any;
    errors: string[];
    constructor(message: string, data: any, errors?: string[]);
}
/**
 * Finds difference of two arrays and returns the difference with a message for each item
 *
 * @param {Array} a - Array to be compared to
 * @param {Array} b - Array to be compared with
 * @param {string} message - message to be shown for the difference
 */
export declare function diff(a: string[], b: string, message: string): SchemaWithWarning[];
export declare function combine(one: string[], two: string[], messages: string[]): SchemaWithWarning[];
/**
 * Walk path and read the contents into an object tree to be used as a shared cache.
 *
 * @param {string} dir - path to be recursively scanned
 * @param {string or Array} include - regex pattern to be matched or an array of file extensions
 *        used for identifying which files are included in the scan. Default includes all files.
 */
export declare function cacheTree(dir: string | undefined, include: any): CachedScripts;
/**
 * Sleeps for a given duration in milliseconds
 *
 * @param {number} milliseconds - milliseconds to sleep
 */
export declare function sleep(milliseconds: number): Promise<unknown>;
/**
 * Transforms a json object using a `jq` filter script.
 *
 * @param {string} filter - jq filter script to be applied
 * @param {any} json - json object on which the filter is applied
 */
export declare function transform(filter: string, json: any): Promise<any>;
