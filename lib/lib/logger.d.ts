import * as pino from "pino";
export declare const logger: pino.Logger;
export declare class TaskDuration {
    task: string;
    initiatedAt: Date;
    completedAt?: Date | undefined;
    duration?: number | undefined;
    data?: object;
    constructor(name: string);
    done(data?: object): TaskDuration;
}
