import { CachedScripts, ConnectionInfo } from './lib/models';
import { ExportStrategy } from './strategy';
export declare function getExportStrategy(dialect: string, connection: ConnectionInfo, scripts: CachedScripts): Promise<ExportStrategy>;
